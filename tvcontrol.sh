#!/bin/bash

exec >> /home/root/tvcontrol.log 2>&1

if [[ $(pgrep -f tvcontrol.sh -c) > 1 ]]; then
 echo "tvcontrol.sh has already started, quitting!" 
 exit 1   
fi

echo Started Script at: $(date)

while true; do

echo "Waiting for commands..."

RES=`echo -e "HTTP/1.1 200 OK\n\n $(date)" | nc -l -p 1515`
#echo $RES
echo Received command at: $(date)

TURNON=`echo $RES | grep -c turnon`
echo Turn on=$TURNON

TURNOFF=`echo $RES | grep -c turnoff`
echo Turn off=$TURNOFF

if [ $TURNON -eq 1 ] && [ $TURNOFF -eq 0 ]
then
 echo "ON"
 wget -O - "http://localhost/web/powerstate?newstate=4"
fi

if [ $TURNON -eq 0 ] && [ $TURNOFF -eq 1 ]
then
 echo "OFF"
 wget -O - "http://localhost/web/powerstate?newstate=5"
fi  

done 
